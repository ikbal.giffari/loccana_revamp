<?php

namespace App\Http\Controllers;

use App\Models\MasterUserModel;
use Illuminate\Http\Request;

class MasterUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['limit'] = 10;
        $data['search'] = $request->search ? $request->search : '';
        $data['page'] = $request->page ? (int)$request->page : 1;
        $data['offset'] = ($data['page'] > 1) ? ($data['page'] * $data['limit']) - $data['limit'] : 0;
        $data['previous'] = $data['page'] - 1;
        $data['next'] = $data['page'] + 1;
        $data['numbering'] = $data['offset'] + 1;

        $data['pagetotal'] = 10;

        for ($i=0; $i < 5; $i++) {
            $data['users'][$i]['id'] = $i+1;
            $data['users'][$i]['name'] = 'Nama'.$i;
            $data['users'][$i]['username'] = 'Username'.$i;
            $data['users'][$i]['role'] = $i;
            $data['users'][$i]['wilayah'] = $i;
            $data['users'][$i]['status'] = 1;

        }
        return view('masterUser.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterUser.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "CREATE USER";
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterUserModel  $masterUserModel
     * @return \Illuminate\Http\Response
     */
    public function show(MasterUserModel $masterUserModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterUserModel  $masterUserModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id, MasterUserModel $masterUserModel)
    {
        $data['user'] = [
            'id' => $id,
            'nama' => 'nama'.$id,
            'alamat' => 'Alamat rumah '.$id,
            'no_telp' => '0812371242',
            'email' => 'email'.$id.'@gmail.com',
            'role' => 2,
            'region' => 3,
            'username' => 'username'.$id,
        ];
        return view('masterUser.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterUserModel  $masterUserModel
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, MasterUserModel $masterUserModel)
    {
        echo "UPDATE USER ID = ".$id;
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterUserModel  $masterUserModel
     * @return \Illuminate\Http\Response
     */
    public function delete($id, MasterUserModel $masterUserModel)
    {
        echo "DELETE USERS ID = ".$id;
    }
}
