@extends('layouts.app')

@section('content')

    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Users</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-dot fw-bold text-gray-600 fs-7 my-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-gray-600">
                        <a href="#" class="text-gray-600 text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-gray-600">Master</li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-gray-500">Users</li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid" id="kt_content">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Add user-->
                        <a href="{{ route('masterUsers.create') }}" class="btn btn-primary">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->
                        <span class="svg-icon svg-icon-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black" />
                                <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->Add User</a>
                        <!--end::Add user-->
                    </div>
                    <!--begin::Card title-->
                    <!--begin::Card toolbar-->
                    <div class="card-toolbar">
                        <!--begin::Toolbar-->
                        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                            <div class="d-flex align-items-center position-relative mt-5">
                                <form action="{{ route('masterUsers.index') }}">
                                <div class="input-group mb-5">
                                        <input type="text" class="form-control" name="search" id="search" placeholder="Search" aria-label="Search" value="{{ $search }}"/>
                                        <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Card toolbar-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                        <!--begin::Table head-->
                        <thead>
                            <!--begin::Table row-->
                            <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                                <th class="min-w-125px">No.</th>
                                <th class="min-w-125px">Nama</th>
                                <th class="min-w-125px">Username</th>
                                <th class="min-w-125px">Role</th>
                                <th class="min-w-125px">Wilayah</th>
                                <th class="min-w-50px">Status</th>
                                <th class="text-center min-w-100px">Actions</th>
                            </tr>
                            <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="text-gray-600 fw-bold">
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $numbering++ }}</td>
                                    <td>{{ $user['name'] }}</td>
                                    <td>{{ $user['username'] }}</td>
                                    <td>{{ $user['role'] }}</td>
                                    <td>{{ $user['wilayah'] }}</td>
                                    <td>{{ $user['status'] }}</td>
                                    <td class="text-center">
                                        <div class="btn-group" role="group">
                                            <a href="{{ route('masterUsers.edit', $user['id']) }}" class="btn btn-icon icon-left btn-warning" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="far fa-edit"></i></a>
                                            <a href="#" class="btn btn-icon icon-left btn-danger" onclick="deleteModal(this)" data-user_id="{{ $user['id'] }}" data-user_name="{{ $user['name'] }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                    <div class="row mt-10">
                        <ul class="pagination">
                            <li class="page-item previous @if($page == 1) disabled @endif">
                                <a href="{{ route('masterUsers.index', 'search='.$search.'&page='.$previous) }}" class="page-link"><i class="previous"></i></a>
                            </li>
                            @php
                                $start = (($page - 2) > 0) ? $page - 2 : 1;
                                $end = (($page + 2 ) < $pagetotal ) ? $page + 2 : $pagetotal;
                                if ($page < 3) {
                                    $end = 5;
                                }
                                if (($start+5) > $pagetotal) {
                                    $start = $pagetotal - 4;
                                }
                            @endphp
                            @for ($x=$start; $x<=$end; $x++)
                            <li class="page-item @if($page == $x) active @endif">
                                <a href="{{ route('masterUsers.index', 'search='.$search.'&page='.$x) }}" class="page-link">{{ $x }}</a>
                            </li>
                            @endfor
                            <li class="page-item next @if($page == $pagetotal) disabled @endif">
                                <a href="{{ route('masterUsers.index', 'search='.$search.'&page='.$next) }}" class="page-link"><i class="next"></i></a>
                            </li>
                        </ul>
                        <label class="text-center my-5">Halaman ke-{{ $page }} dari {{ $pagetotal }}</label>
                    </div>
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->

    {{-- <button type="button" class="btn btn-primary">
        Launch demo modal
    </button> --}}

    <div class="modal fade" tabindex="-1" id="deleteModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Warning</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <form id="deleteForm" action="" method="POST">
                    @method('delete')
                    @csrf
                    <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script type="text/javascript">

        document.getElementById('deleteButton').addEventListener("click", function(e) {
            document.getElementById('deleteButton').innerHTML = "Executing...";
            document.getElementById('deleteButton').disabled = true;
            deleteForm.submit();
        });

        function deleteModal(data) {
            var user_id = $(data).data('user_id');
            var user_name = $(data).data('user_name');
            var formAction = "{{ url('master/users') }}/" + user_id;
            document.getElementById('deleteForm').action = formAction;
            $('#deleteModal .modal-body').html('<p>Anda yakin akan menghapus Users <strong>' + user_name.toUpperCase() + '</strong>?');
            $('#deleteModal').modal('show')
        }

    </script>
    @endpush

@endsection
