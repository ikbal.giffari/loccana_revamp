@extends('layouts.app')

@section('content')

    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Tambah User</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-dot fw-bold text-gray-600 fs-7 my-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-gray-600">
                        <a href="#" class="text-gray-600 text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    @php
                        $numItems = count(request()->segments());
                        $i = 0;
                    @endphp
                    <!--begin::Item-->
                    @foreach (request()->segments() as $item)
                        @if (ucwords($item) == 'Create')
                            <li class="breadcrumb-item {{ ++$i == $numItems ? 'text-gray-500' : 'text-gray-600' }}">
                                Tambah user
                            </li>
                        @elseif (ucwords($item) == 'Edit')
                            <li class="breadcrumb-item text-gray-500">
                                Edit user
                            </li>
                        @elseif (is_numeric($item))
                            @continue
                        @else
                            <li class="breadcrumb-item {{ ++$i == $numItems ? 'text-gray-500' : 'text-gray-600' }}">
                                {{ ucwords($item) }}
                            </li>
                        @endif
                    @endforeach
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid" id="kt_content">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card body-->
                <div class="card-body">
                    <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
                    <form action="{{ route('masterUsers.store') }}" method="POST" class="my-7">
                        @csrf
                        <div class="row mx-10">
                            <div class="col-md-6">
                                <!--begin::Input group-->
                                <div class="mb-3">
                                    <!--begin::Label-->
                                    <label class="required fw-bold fs-6 mb-2">Nama</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input type="text" name="name" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="Nama" required/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-3">
                                    <!--begin::Label-->
                                    <label class="required fw-bold fs-6 mb-2">Alamat</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input type="text" name="alamat" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="Alamat" required/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-3">
                                    <!--begin::Label-->
                                    <label class="required fw-bold fs-6 mb-2">No. Telp</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input type="text" name="no_telp" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="No. Telp" required/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-3">
                                    <!--begin::Label-->
                                    <label class="required fw-bold fs-6 mb-2">Email</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input type="email" name="email" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="Email" required/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                            </div>
                            <div class="col-md-6">
                                <!--begin::Input group-->
                                <div class="mb-3">
                                    <!--begin::Label-->
                                    <label class="required fw-bold fs-6 mb-2">Role</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <select name="role" id="role" class="form-select form-select-solid mb-3 mb-lg-0" required>
                                        <option value="">-- Pilih Role --</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-3">
                                    <!--begin::Label-->
                                    <label class="required fw-bold fs-6 mb-2">Region</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <select name="region" id="region" class="form-select form-select-solid mb-3 mb-lg-0" required>
                                        <option value="">-- Pilih Region --</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                    <!--end::Input-->
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-3">
                                    <!--begin::Label-->
                                    <label class="required fw-bold fs-6 mb-2">Username</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input type="text" name="username" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="Username" required/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-3">
                                    <!--begin::Label-->
                                    <label class="required fw-bold fs-6 mb-2">Password</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input type="password" name="password" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="Password" required/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                            </div>
                            <div class="text-center mt-10">
                                <button type="button" id="batal" class="btn btn-light me-3">Cancel</button>
                                <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->

@endsection
