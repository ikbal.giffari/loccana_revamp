<!--begin::Header-->
<div id="kt_header" class="header" data-kt-sticky="true" data-kt-sticky-name="header" data-kt-sticky-offset="{default: '200px', lg: '300px'}">
    <!--begin::Container-->
    <div class="container-xxl d-flex flex-grow-1 flex-stack">
        <!--begin::Header Logo-->
        <div class="d-flex align-items-center me-5">
            <!--begin::Heaeder menu toggle-->
            <div class="d-lg-none btn btn-icon btn-active-color-primary w-30px h-30px ms-n2 me-3" id="kt_header_menu_toggle">
                <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
                        <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
                    </svg>
                </span>
                <!--end::Svg Icon-->
            </div>
            <!--end::Heaeder menu toggle-->
            <a href="../../demo11/dist/index.html">
                <img alt="Logo" src="{{ asset('assets/media/logos/logo-demo11.svg') }}" class="h-20px h-lg-30px" />
            </a>
        </div>
        <!--end::Header Logo-->
        <!--begin::Topbar-->
        <div class="d-flex align-items-center">
            <!--begin::Topbar-->
            <div class="d-flex align-items-center flex-shrink-0">
                <!--begin::User-->
                <div class="d-flex align-items-center ms-3 ms-lg-4" id="kt_header_user_menu_toggle">
                    <!--begin::Menu- wrapper-->
                    <!--begin::User icon(remove this button to use user avatar as menu toggle)-->
                    <div class="btn btn-icon btn-color-gray-700 btn-active-color-primary btn-outline btn-outline-secondary w-30px h-30px w-lg-40px h-lg-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                        <!--begin::Svg Icon | path: icons/duotune/communication/com013.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::User icon-->
                    <!--begin::Menu-->
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
                        <!--begin::Menu item-->
                        <div class="menu-item px-3">
                            <div class="menu-content d-flex align-items-center px-3">
                                <!--begin::Avatar-->
                                <div class="symbol symbol-50px me-5">
                                    <img alt="Logo" src="{{ asset('assets/media/avatars/150-26.jpg') }}" />
                                </div>
                                <!--end::Avatar-->
                                <!--begin::Username-->
                                <div class="d-flex flex-column">
                                    <div class="fw-bolder d-flex align-items-center fs-5">Max Smith
                                    <span class="badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2">Pro</span></div>
                                    <a href="#" class="fw-bold text-muted text-hover-primary fs-7">max@kt.com</a>
                                </div>
                                <!--end::Username-->
                            </div>
                        </div>
                        <!--end::Menu item-->
                        <!--begin::Menu separator-->
                        <div class="separator my-2"></div>
                        <!--end::Menu separator-->
                        <!--begin::Menu item-->
                        <div class="menu-item px-5">
                            <a href="../../demo11/dist/account/overview.html" class="menu-link px-5">My Profile</a>
                        </div>
                        <!--end::Menu item-->
                        <!--begin::Menu separator-->
                        <div class="separator my-2"></div>
                        <!--end::Menu separator-->
                        <!--begin::Menu item-->
                        <div class="menu-item px-5">
                            <a href="../../demo11/dist/authentication/flows/basic/sign-in.html" class="menu-link px-5">Sign Out</a>
                        </div>
                        <!--end::Menu item-->
                    </div>
                    <!--end::Menu-->
                    <!--end::Menu wrapper-->
                </div>
                <!--end::User -->
            </div>
            <!--end::Topbar-->
        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Container-->
    <!--begin::Separator-->
    <div class="separator"></div>
    <!--end::Separator-->
    <!--begin::Container-->
    <div class="header-menu-container container-xxl d-flex flex-stack h-lg-75px" id="kt_header_nav">
        <!--begin::Menu wrapper-->
        <div class="header-menu flex-column flex-lg-row" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_header_menu_toggle" data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
            <!--begin::Menu-->
            <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch flex-grow-1" id="#kt_header_menu" data-kt-menu="true">
                <div class="menu-item me-lg-1">
                    <a class="menu-link {{ Request::segment(1) == 'dashboard' ? 'active' : ''}} py-3" href="{{ route('dashboard.index') }}">
                        <span class="menu-title">Dashboard</span>
                        <span class="menu-arrow d-lg-none"></span>
                    </a>
                </div>
                <!--begin::Menu Master Data-->
                <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                    <span class="menu-link {{ Request::segment(1) == 'master' ? 'active' : ''}} py-3">
                        <span class="menu-title">Master Data</span>
                        <span class="menu-arrow d-lg-none"></span>
                    </span>
                    <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                        <div class="menu-item">
                            <a class="menu-link {{ Request::segment(2) == 'users' ? 'active' : ''}} py-3" href="{{ route('masterUsers.index') }}" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Users</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Items</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">UOM</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Price</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Partners</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Informasi</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">COA</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Menu Master Data-->

                <!--begin::Menu Procurement-->
                <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                    <span class="menu-link py-3">
                        <span class="menu-title">Procurement</span>
                        <span class="menu-arrow d-lg-none"></span>
                    </span>
                    <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Purchase Order</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Goods Receipt</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Invoice</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Retur</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Menu Procurement-->

                <!--begin::Menu Inventory-->
                <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                    <span class="menu-link py-3">
                        <span class="menu-title">Inventory</span>
                        <span class="menu-arrow d-lg-none"></span>
                    </span>
                    <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Stock</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Stock in Transit</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Menu Inventory-->

                <!--begin::Menu Selling-->
                <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                    <span class="menu-link py-3">
                        <span class="menu-title">Selling</span>
                        <span class="menu-arrow d-lg-none"></span>
                    </span>
                    <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Selling</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Range Price Management</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Retur</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Menu Selling-->

                <!--begin::Menu Cash Bank-->
                <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                    <span class="menu-link py-3">
                        <span class="menu-title">Cash Bank</span>
                        <span class="menu-arrow d-lg-none"></span>
                    </span>
                    <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Debt</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Receivables</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Menu Cash Bank-->

                <!--begin::Menu Accounting-->
                <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                    <span class="menu-link py-3">
                        <span class="menu-title">Accounting</span>
                        <span class="menu-arrow d-lg-none"></span>
                    </span>
                    <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Adjusting Journal Entry</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Asset</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Profit and Loss</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Balance</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Income Journal</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Journal of Expenditure</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Menu Accounting-->

                <!--begin::Menu Reporting-->
                <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                    <span class="menu-link py-3">
                        <span class="menu-title">Reporting</span>
                        <span class="menu-arrow d-lg-none"></span>
                    </span>
                    <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Cash</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Debt</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Receivables</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Large Book</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Basic Selling</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Selling</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Stock</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Basic Purchase</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link py-3" href="#" data-bs-dismiss="click" data-bs-placement="right">
                                <span class="menu-icon">
                                </span>
                                <span class="menu-title">Report Purchase</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Menu Reporting-->
            </div>
            <!--end::Menu-->
        </div>
        <!--end::Menu wrapper-->
    </div>
    <!--end::Container-->
</div>
<!--end::Header-->
